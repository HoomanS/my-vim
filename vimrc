" vimrc - Configuation file for vim
"
" Author: Hamza Rihani

" To disable a plugin, add it's bundle name to the following list
let g:pathogen_disabled = []
" NERDTree is somehow too slow, disable it for now
" call add(g:pathogen_disabled, 'nerdtree')
call add(g:pathogen_disabled, 'calendar.vim')
call add(g:pathogen_disabled, 'gv-vim')
"call add(g:pathogen_disabled, 'conque_term')
" Gundo requires at least vim 7.3
if v:version < '703' && (!has('python') || !has('python3'))
        call add(g:pathogen_disabled, 'gundo')
endif
execute pathogen#infect()

syntax on
filetype on
filetype plugin on
set backspace=indent,eol,start
set noeb vb t_vb= " disabling bells
set cursorcolumn
set showcmd
set showmatch
set wildignorecase  " Ignore case when completing file names and directories. vim >7.3
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab
set wildmode=longest,list,full
set wildmenu
" set encoding=utf-8
set modeline        " activate the modeline
"set list
"set listchars=nbsp:.,tab:›\ ,trail:¤,extends:>,precedes:<,eol:\
"set expandtab
set hidden " activate hidden buffers
set nu
set laststatus=2
set sessionoptions=blank,buffers,curdir,globals,help,localoptions,options,resize,tabpages,winsize,winpos
set t_ZH=[3m " to get italics
set t_ZR=[23m " to get italics
set ttimeoutlen=10 " timeout when leaving insert mode
au VimLeave * :!clear
" ----- Colorscheme customization
set t_Co=256
let g:airline_theme='bubblegum' " airline config comes before colorscheme
"-- Solarized colorscheme
"set termguicolors
set background=dark

" Solarized colorscheme " {{{1
let g:solarized_contrast="low"
"let g:solarized_termcolors=256
"let g:solarized_diffmode="high"
let g:solarized_termtrans=1
let g:solarized_term_italic=1
let g:solarized_statusline="high"
"let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
"let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
colorscheme solarized8_high
set cursorline
hi SpellBad ctermfg=172 cterm=NONE,reverse,underline
hi CursorLineNr cterm=None, ctermfg=004
hi CursorLine cterm=NONE ctermbg=None
" }}}1

"-- Badwolf colorscheme "{{{1
"let g:badwolf_tabline = 3 " more visible colors for the tabline
"colorscheme badwolf
"set background=light
"set background=dark
"hi Normal ctermbg=none
"hi Visual cterm=reverse
"highlight NonText ctermbg=none
"highlight Text ctermbg=none
"hi Folded ctermfg=lightgrey term=bold cterm=bold

" ------ Begin diff color section
"if &diff " load different scheme if vimdiff is called
"    colorscheme apprentice
"endif
"" ------ Diff colors optained from Apprentice colorscheme by Romain Lafourcade
"hi DiffAdd    ctermbg=235  ctermfg=108  guibg=#262626 guifg=#87af87 cterm=reverse gui=reverse
"hi DiffChange ctermbg=235  ctermfg=103  guibg=#262626 guifg=#8787af cterm=reverse gui=reverse
"hi DiffDelete ctermbg=235  ctermfg=131  guibg=#262626 guifg=#af5f5f cterm=reverse gui=reverse
"hi DiffText   ctermbg=235  ctermfg=208  guibg=#262626 guifg=#ff8700 cterm=reverse gui=reverse
" ------ End diff color section
" }}}1

"-- Highlight comments in italics
hi Comment cterm=italic

" ----- Completion menu colors
"highlight Pmenu ctermfg=black ctermbg=white
"highlight PmenuSel ctermfg=black ctermbg=lightyellow

" ----- Tab color customization "{{{1
hi TabLine      ctermfg=White  ctermbg=238     cterm=NONE
hi TabLineFill  ctermfg=White  ctermbg=238 cterm=NONE
hi TabLineSel   ctermfg=Black  ctermbg=DarkBlue  cterm=NONE
"}}}1

" ============================
"       User mappings
" ============================
" Usage: "{{{1
" map space to \ (default leader)
map <Space> \
" add mapping to view and jump in buffers
nnoremap <leader>b <ESC>:ls<CR>:b<Space>
" ----- Map kj to <Esc> in insert mode
imap kj <Esc>
" ----- No VISUAL mode
nnoremap Q <Nop>
" ----- Toggle between numbers and relative numbers
function! ToggleNumber()
    if(&relativenumber == 1)
        set norelativenumber
        set number
    else
        set relativenumber
    endif
endfunc
map <F5> <Esc>:call ToggleNumber()<CR>
" ----- Toggle quickfix window
function! ToggleQuickFix()
    if exists("g:qf_opened")
        cclose
        unlet g:qf_opened
    else
        botright copen
        let g:qf_opened=1
    endif
endfunc
map <leader>ct <Esc>:call ToggleQuickFix()<CR>
"}}}1

" Spell: checking options "{{{1
"let g:languagetool_jar='$HOME/.vim/LanguageTool-2.8/languagetool.jar'
let g:languagetool_jar='$HOME/.vim/LanguageTool-2.8/languagetool-commandline.jar'
map <silent> <F7> <Esc>:silent setlocal spell! spelllang=en_us<CR>
map <silent> <F6> <Esc>:silent setlocal spell! spelllang=fr<CR>
"}}}1
" ============================
"     Plugin customization
" ============================
" YouCompleteMe: "{{{1
let g:ycm_global_ycm_extra_conf = "~/.vim/ycm_extra_conf.py"
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_autoclose_preview_window_after_completion = 1
"let g:ycm_filetype_blacklist = { 'tex': 1 }
"}}}1

" Coding Style: "{{{1
let g:ccstyle_linux_patterns = ["/usr/src/", "/linux", "bxi-modules/",  "bxi-portals/", "libfabric-bxi-portals"]
let g:ccstyle_knf_patterns = []
"}}}1

" Gitv:  a gitk clone in Vim "{{{1
let g:Gitv_WipeAllOnClose = 0 " close buffers opened by gitv

function! GitvStandAlone()
" call this to run gitv in vim directly from the terminal
    call fugitive#detect(getcwd())
    if exists('b:git_dir')
      Gitv
    else
      let i = input("Not Git repository")
      redraw!
      quit
    endif
    bd 1
endfunction
"}}}1

" Statusline: Powerline and Airline plugins options (for a fancy statusline) "{{{1
"let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
let g:airline_symbols.space = "\ua0"
" TODO find a better way to check for fonts
if !has('gui_running') && (hostname() == 'santel' || hostname() == 'echelle')
" If Poweline fonts are available
  let g:airline_left_sep = ''
  let g:airline_left_alt_sep = ''
  let g:airline_right_sep = ''
  let g:airline_right_alt_sep = ''
  let g:airline_symbols.branch = ''
  let g:airline_symbols.readonly = ''
  let g:airline_symbols.linenr = ''
else
" unicode symbols
  let g:airline_left_sep = '»'
  let g:airline_left_sep = '▶'
  let g:airline_right_sep = '«'
  let g:airline_right_sep = '◀'
  let g:airline_symbols.crypt = '🔒'
  let g:airline_symbols.linenr = '␊'
  let g:airline_symbols.linenr = '␤'
  let g:airline_symbols.linenr = '¶'
  let g:airline_symbols.maxlinenr = '☰'
  let g:airline_symbols.maxlinenr = ''
  let g:airline_symbols.branch = '⎇'
  let g:airline_symbols.paste = 'ρ'
  let g:airline_symbols.paste = 'Þ'
  let g:airline_symbols.paste = '∥'
  let g:airline_symbols.spell = 'Ꞩ'
  let g:airline_symbols.notexists = '∄'
  let g:airline_symbols.whitespace = 'Ξ'
endif
"let g:airline_theme='bubblegum' " moved to before the colorscheme
"}}}1

" Gundo: Graph Vim Undo Tree "{{{1
nnoremap <F4> :GundoToggle<CR>
"let g:gundo_auto_preview = 0
"}}}1

" NERDTree: File explorer {{{1
map <C-n> :NERDTreeToggle<CR>
map <leader>n :NERDTreeMirror<CR>
" exit vim if the only left is NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" start vim with NERDTree if no argument is provided
"autocmd StdinReadPre * let s:std_in=1
"autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
"}}}1

" Calendar: plugin options {{{1
let g:calendar_frame = 'default'
" Map clock in horizontal preview to <F3>
map <F3> :Calendar -view=clock -split=horizontal -height=15<CR>
"}}}1

" Vimtex: plugin options {{{1
"  ---- enabling YouCompleteMe for vimtex
  if !exists('g:ycm_semantic_triggers')
    let g:ycm_semantic_triggers = {}
  endif
  let g:ycm_semantic_triggers.tex = [
        \ 're!\\[A-Za-z]*cite[A-Za-z]*(\[[^]]*\]){0,2}{[^}]*',
        \ 're!\\[A-Za-z]*ref({[^}]*|range{([^,{}]*(}{)?))',
        \ 're!\\hyperref\[[^]]*',
        \ 're!\\includegraphics\*?(\[[^]]*\]){0,2}{[^}]*',
        \ 're!\\(include(only)?|input){[^}]*',
        \ 're!\\\a*(gls|Gls|GLS)(pl)?\a*(\s*\[[^]]*\]){0,2}\s*\{[^}]*',
        \ 're!\\includepdf(\s*\[[^]]*\])?\s*\{[^}]*',
        \ 're!\\includestandalone(\s*\[[^]]*\])?\s*\{[^}]*',
        \ ]

let g:vimtex_latexmk_build_dir = 'build' " generate files in a subfile build/
let g:vimtex_compiler_latexmk = {'build_dir':'build'} " generate files in a subfile build/
let g:tex_flavor = 'latex' " avoid detecting the files as plaintex
let g:vimtex_quickfix_open_on_warning = 0 " disable automatic quickfix opening for warnings
let g:tex_comment_nospell = 1 " disable spell checker for comments
"}}}1

" Run test a selection in shell. seen on:
"https://stackoverflow.com/questions/1237780/vim-execute-shell-command-without-filtering
":command-range Test :w !sh

" (Based on http://stackoverflow.com/questions/5927952/whats-implementation-of-vims-default-tabline-function)
" Modification of tabs " {{{1
"if exists("+showtabline")
"    function! MyTabLine()
"        let s = ''
"        let wn = ''
"        let t = tabpagenr()
"        let i = 1
"        while i <= tabpagenr('$')
"            let buflist = tabpagebuflist(i)
"            let winnr = tabpagewinnr(i)
"            let s .= '%' . i . 'T'
"            let s .= (i == t ? '%1*' : '%2*')
"            let s .= ' '
"            let wn = tabpagewinnr(i,'$')
"
"            let s .= '%#TabNum#'
"            let s .= i
"            " let s .= '%*'
"            let s .= (i == t ? '%#TabLineSel#' : '%#TabLine#')
"            let bufnr = buflist[winnr - 1]
"            let file = bufname(bufnr)
"            let buftype = getbufvar(bufnr, 'buftype')
"            if buftype == 'nofile'
"                if file =~ '\/.'
"                    let file = substitute(file, '.*\/\ze.', '', '')
"                endif
"            else
"                let file = fnamemodify(file, ':p:t')
"            endif
"            if file == ''
"                let file = '[No Name]'
"            endif
"            let s .= ' ' . file . ' '
"            let i = i + 1
"        endwhile
"        let s .= '%T%#TabLineFill#%='
"        let s .= (tabpagenr('$') > 1 ? '%999XX' : 'X')
"        return s
"    endfunction
"    set stal=2
"    set tabline=%!MyTabLine()
"    set showtabline=1
"    highlight link TabNum Special
"endif

" ---------------------------------------------------------------------------------------------------------------
" ----- Customize statusline
"function! InsertStatuslineColor(mode)
"  if a:mode == 'i'
"    "hi statusline guibg=LightGreen ctermfg=0 guifg=Black ctermbg=3 cterm=bold
"    hi statusline  ctermfg=16 ctermbg=33
"  elseif a:mode == 'r'
"    hi statusline ctermfg=5 ctermbg=0
"  else
"    hi statusline guibg=DarkRed ctermfg=1 guifg=Black ctermbg=0
"  endif
"endfunction
"
"au InsertEnter * call InsertStatuslineColor(v:insertmode)
"au InsertLeave * hi statusline guibg=DarkGrey ctermfg=8 guifg=White ctermbg=7
" default the statusline to green when entering Vim
"hi statusline guibg=DarkGrey ctermfg=8 guifg=White ctermbg=15 cterm=reverse
"hi statusline ctermfg=15 ctermbg=0

"au InsertEnter * hi statusline  ctermfg=16 ctermbg=221 cterm=bold
"au InsertLeave * hi statusline  ctermfg=16 ctermbg=33 cterm=bold

"}}}1

" Modifying status line "{{{1
""set statusline=%<%f%m\ \[%{&ff}:%{&fenc}:%Y]\ %{getcwd()}\ \ \[%{strftime('%Y/%b/%d\ %a\ %I:%M\ %p')}\]\ %=\ Line:%l\/%L\ Column:%c%V\ %P
"set statusline=
"set statusline+=\[%n]                                           "buffernr
"set statusline+=\ %<%F\                                         "File+path
"set statusline+=\ \[%Y\]\                                       "FileType
"set statusline+=\ \[%{''.(&fenc!=''?&fenc:&enc).''}             "Encoding
"set statusline+=\ %{(&bomb?\",BOM\":\"\")}\                     "Encoding2
"set statusline+=\ %{&ff}\                                       "FileFormat (dos/unix..)
"set statusline+=\ %{&spelllang}\]\                              "Spellanguage & Highlight on?
"set statusline+=\ \[%{strftime('%Y/%b/%d\ %a\ %I:%M\ %p')}\]\   "Date and time
"set statusline+=\ %=\ row:%l/%L\                                "Rownumber/total
"set statusline+=\ col:%03c\                                     "Colnr
"set statusline+=\ \ %m%r%w\ %P\ \                               "Modified? Readonly? Top/bot.
"}}}1
"
" vim: fdm=marker sw=2
